#!/bin/sh

echo "Provisioning virtual machine..."

echo "Configure localtime"
if [ -z $TZ ]; then
  TZ="Europe/Paris"
fi

cat <<EOF | tee /etc/environment
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

rm /etc/localtime && ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime

echo "Configure access to root account via public key"
mkdir -p /root/.ssh
cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

echo "Adds alma user"
adduser alma || true
mkdir -p /home/alma/.ssh
cp /home/vagrant/.ssh/authorized_keys /home/alma/.ssh/
chown -R alma:alma /home/alma/.ssh
echo "%alma ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/alma

echo "Adds additionnal packages"
yum install -y glibc-langpack-en

cat <<EOF >> /etc/hosts
10.42.0.11 docker-master-1
10.42.0.12 docker-master-2
10.42.0.13 docker-master-3
10.42.0.21 docker-worker-1
10.42.0.22 docker-worker-2
10.42.0.23 docker-worker-3
EOF

echo "Done"

