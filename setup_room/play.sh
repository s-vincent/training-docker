#!/bin/sh

ROOM_USER=user

if [ -z "${ROOM_PASSWORD}" ]; then
  read -s -p "Enter password: " ROOM_PASSWORD
  export ROOM_PASSWORD
fi

if [ ! -f ~/.ssh/id_rsa ]; then
    ./init-trainer-computer.sh
fi

ansible-playbook -i inventory/hosts -u "${ROOM_USER}" playbook.yml "$@" -e ansible_password='{{ lookup("env", "ROOM_PASSWORD") }}'  -e ansible_become_pass='{{ lookup("env", "ROOM_PASSWORD") }}'
